﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nosnosc.Utils;
using System.IO;
using System.Data.SqlClient;
using Nosnosc.Profiles;


namespace Nosnosc
{
    public partial class Form1 : Form
    {
        //Kontrola ilości metod
        Metody_obliczeniowe metody_obliczeniowe;
        Metody_obliczeniowe z;
        ValueProfiles obliczenia;

        public Form1()
        {
            InitializeComponent();

            //Kontrola ilości metod - implementacja enum Items
            comboBox1.Items.Clear();
            foreach (string item in Enum.GetNames(typeof(Opcje)))
                comboBox1.Items.Add(item);
            metody_obliczeniowe = new Metody_obliczeniowe(Convert.ToDouble(textBox_L.Text),
                Convert.ToDouble(textBox_P.Text), Convert.ToDouble(textBox_Q.Text), Convert.ToDouble(textBox_M.Text));

            if (comboBox1.Items.Count > -1)
                comboBox1.SelectedIndex = 0;

            if (selectSection.Items.Count > -1)
                selectSection.SelectedIndex = 0;

            if (ClassOfSteal.Items.Count > -1)
                ClassOfSteal.SelectedIndex = 0;

            FillDropDowns();

            richTextBoxMcrd.Rtf = @"{\rtf\deflang{\font}\nosupersub M\sub\ c.Rd =}";
            richTextBoxNrd.Rtf = @"{\rtf\deflang{\font}\nosupersub N\sub\ pl.Rd =}";
            richTextBoxNrds.Rtf = @"{\rtf\deflang{\font}\nosupersub N\sub\ c.Rd =}";
            richTextBoxVrd.Rtf = @"{\rtf\deflang{\font}\nosupersub V\sub\ pl.Rd =}";
        }

        #region Otwarcie połaczenia sql załadowanie bazy profili 
        private void FillDropDowns()
        {
            //string commonFolder = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);


            string exe = System.Reflection.Assembly.GetExecutingAssembly().Location;
            string commonFolder = System.IO.Path.GetDirectoryName(exe);
            AppDomain.CurrentDomain.SetData("DataDirectory", commonFolder);

            using (SqlConnection connection = new SqlConnection("Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\\Profile.mdf;Integrated Security=True"))
            {
                connection.Open();
                //Przekazanie listy profili z GetIPNs do profiles_2
                List<string> profiles_2 = ValueProfiles.GetIPNs(connection);
                //Dodanie poszczególnych elementów kolekcji
                foreach (string x in profiles_2)
                    TypeOfProfiles.Items.Add(x);
            }
        }
        #endregion

        #region Obsuga ComboBox
        // ComboBox ustawienie ilustracji/wybór początkowy
        private void comboBox1_DrawItem(object sender, DrawItemEventArgs e)
        {
            if (e.Index > -1)
                e.Graphics.DrawImage(imageList1.Images[e.Index], e.Bounds.Left, e.Bounds.Top);

        }

        //Przypisanie wielkości obiektu
        private void comboBox1_MeasureItem(object sender, MeasureItemEventArgs e)
        {
            e.ItemHeight = imageList1.ImageSize.Height;
            e.ItemWidth = imageList1.ImageSize.Width;
        }

        //Ustawienie wielkości pola wyboru
        private void Form1_Load(object sender, EventArgs e)
        {
            comboBox1.ItemHeight = imageList1.ImageSize.Height;
            comboBox1.Width = imageList1.ImageSize.Width + 25;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Opcje w = (Opcje)Enum.Parse(typeof(Opcje), (string)((ComboBox)sender).SelectedItem);
            wybor(w);
        }
        #endregion

        #region Definicja pobieranych znaków - tylko liczby

        private void textBox_L_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funkcje_kontrolek.Wpisywanie_Liczby(sender, e);
        }

        private void textBox_P_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funkcje_kontrolek.Wpisywanie_Liczby(sender, e);
        }

        private void textBox_Q_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funkcje_kontrolek.Wpisywanie_Liczby(sender, e);
        }

        private void textBox_M_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funkcje_kontrolek.Wpisywanie_Liczby(sender, e);
        }

        private void textBox_S_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            Funkcje_kontrolek.Wpisywanie_Liczby(sender, e);
        }

        #endregion

        #region Widocznosc w ComboBox
        // Widoczność w ComboBox
        public void wybor(Opcje param)
        {
            if (param == Opcje.Sila_skupiona)
            {
                czysc();
                textBox_P.Visible = true;
                textBox_Q.Visible = false;
                textBox_M.Visible = false;
                label2.ForeColor = Color.Black;
                label4.ForeColor = Color.LightSlateGray;
                label5.ForeColor = Color.LightSlateGray;
            }
            else if (param == Opcje.Sila_rozłożona)
            {
                czysc();
                textBox_P.Visible = false;
                textBox_Q.Visible = true;
                textBox_M.Visible = false;
                label2.ForeColor = Color.LightSlateGray;
                label4.ForeColor = Color.Black;
                label5.ForeColor = Color.LightSlateGray;
            }
            else if (param == Opcje.Po_trojkacie)
            {
                czysc();
                textBox_P.Visible = false;
                textBox_Q.Visible = true;
                textBox_M.Visible = false;
                label2.ForeColor = Color.LightSlateGray;
                label4.ForeColor = Color.Black;
                label5.ForeColor = Color.LightSlateGray;
            }
            else if (param == Opcje.Moment)
            {
                czysc();
                textBox_P.Visible = false;
                textBox_Q.Visible = false;
                textBox_M.Visible = true;
                label2.ForeColor = Color.LightSlateGray;
                label4.ForeColor = Color.LightSlateGray;
                label5.ForeColor = Color.Black;
            }
        }
        #endregion

        #region  Obliczenia
        private void button1_Click(object sender, EventArgs e)
        {
            ObiektWyniku wynik = null;

            if (comboBox1.SelectedItem != null)
            {
                Opcje w = (Opcje)Enum.Parse(typeof(Opcje), (string)comboBox1.SelectedItem);
                Metody_obliczeniowe metody_obliczeniowe = null;

                #region Przypisywanie parametrow i metod
                //Wybór parametrów i metody
                switch (w)
                {
                    case Opcje.Moment:
                        //Sprawdzenie czy wartość zadana nie jest pusta
                        if (!string.IsNullOrEmpty(textBox_M.Text) && !string.IsNullOrEmpty(textBox_L.Text) && textBox_M.Text != "0" && textBox_L.Text != "0")
                        {
                            //Jezeli wszystko ok tworzymy instancję klasy i przekazujemy zmienne
                            metody_obliczeniowe = new Metody_obliczeniowe()
                            {
                                M = Convert.ToDouble(textBox_M.Text),
                                L = Convert.ToDouble(textBox_L.Text)
                            };
                        }
                        else
                            MessageBox.Show("Nie wprowadzono wymaganych parametrów!", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                        break;
                    case Opcje.Po_trojkacie:
                        if (!string.IsNullOrEmpty(textBox_Q.Text) && !string.IsNullOrEmpty(textBox_L.Text) && textBox_Q.Text != "0" && textBox_L.Text != "0")
                        {
                            metody_obliczeniowe = new Metody_obliczeniowe()
                            {
                                Q = Convert.ToDouble(textBox_Q.Text),
                                L = Convert.ToDouble(textBox_L.Text)

                            };
                        }
                        else
                            MessageBox.Show("Nie wprowadzono wymaganych parametrów!", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                        break;
                    case Opcje.Sila_rozłożona:
                        if (!string.IsNullOrEmpty(textBox_Q.Text) && !string.IsNullOrEmpty(textBox_L.Text) && textBox_Q.Text != "0" && textBox_L.Text != "0")
                        {
                            metody_obliczeniowe = new Metody_obliczeniowe()
                            {
                                Q = Convert.ToDouble(textBox_Q.Text),
                                L = Convert.ToDouble(textBox_L.Text)
                            };
                        }
                        else
                            MessageBox.Show("Nie wprowadzono wymaganych parametrów!", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                        break;
                    case Opcje.Sila_skupiona:
                        if (!string.IsNullOrEmpty(textBox_P.Text) && !string.IsNullOrEmpty(textBox_L.Text) && textBox_P.Text != "0" && textBox_L.Text != "0")
                        {
                            metody_obliczeniowe = new Metody_obliczeniowe()
                            {
                                P = Convert.ToDouble(textBox_P.Text),
                                L = Convert.ToDouble(textBox_L.Text)
                            };
                        }
                        else
                            MessageBox.Show("Nie wprowadzono wymaganych parametrów!", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                        break;
                }
                #endregion

                //Wykonaj obliczenia instancja klasy nie jest pusta
                if (metody_obliczeniowe != null)
                {
                    wynik = metody_obliczeniowe.Oblicz(w);
                    WprowadzWyniki(wynik);
                }
            }


            try
            {
                if (ClassOfSteal.SelectedItem == "S235")
                {
                    decimal fy = 235;
                    decimal S = Convert.ToDecimal(textBox_S.Text);
                    z = new Metody_obliczeniowe(this.obliczenia, fy, S);
                    nosnoscWytezenie(z.NosnoscMc(wynik.Moment_1));
                    nosnoscWytezenieVc(z.NosnoscNc(wynik.Sila_tnaca_1, wynik.Sila_tnaca_2));
                    nosnoscWytezenieSc(z.NosnoscSciskanie_2());
                    wypiszNosnosciPrzekroju();
                }

                else if (ClassOfSteal.SelectedItem == "S275")
                {
                    decimal fy = 275;
                    decimal S = Convert.ToDecimal(textBox_S.Text);
                    z = new Metody_obliczeniowe(this.obliczenia, fy, S);
                    nosnoscWytezenie(z.NosnoscMc(wynik.Moment_1));
                    nosnoscWytezenieVc(z.NosnoscNc(wynik.Sila_tnaca_1, wynik.Sila_tnaca_2));
                    nosnoscWytezenieSc(z.NosnoscSciskanie_2());
                    wypiszNosnosciPrzekroju();
                }

                else if (ClassOfSteal.SelectedItem == "S355")
                {
                    decimal fy = 355;
                    decimal S = Convert.ToDecimal(textBox_S.Text);
                    z = new Metody_obliczeniowe(this.obliczenia, fy, S);
                    nosnoscWytezenie(z.NosnoscMc(wynik.Moment_1));
                    nosnoscWytezenieVc(z.NosnoscNc(wynik.Sila_tnaca_1, wynik.Sila_tnaca_2));
                    nosnoscWytezenieSc(z.NosnoscSciskanie_2());
                    wypiszNosnosciPrzekroju();
                }
            }

            catch
            {
                MessageBox.Show("Uzupełnij dane !", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        #endregion

        //Czyszczenie kontrolek
        public void czysc()
        {
            textBox_L.Text = textBox_P.Text = textBox_Q.Text = textBox_M.Text = "0";
            label9.Text = label10.Text = label11.Text = null;
        }

        public void WprowadzWyniki(ObiektWyniku wynik)
        {
            label9.Text = wynik.Moment_1.ToString("0.00");
            label10.Text = wynik.Sila_tnaca_1.ToString("0.00");
            label11.Text = wynik.Sila_tnaca_2.ToString("0.00");
        }

        private void TypeOfProfiles_SelectedIndexChanged(object sender, EventArgs e) //jeżeli wybieram element typ profilu
        {
            string exe = System.Reflection.Assembly.GetExecutingAssembly().Location;
            string commonFolder = System.IO.Path.GetDirectoryName(exe);
            AppDomain.CurrentDomain.SetData("DataDirectory", commonFolder);
            using (SqlConnection connection = new SqlConnection("Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\\Profile.mdf;Integrated Security=True"))
            {
                connection.Open();
                this.obliczenia = ValueProfiles.GetIPNbyIPN(connection, TypeOfProfiles.SelectedItem as string); //Przekazuje wybrany element do metody GetIPNbyIPN w ValueProfiles         
                listView1.Items.Clear();
                if (this.obliczenia != null)
                    listView1.Items.AddRange(this.obliczenia.ToArray());
            }
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            ValueProfiles.section = selectSection.SelectedItem.ToString();
            TypeOfProfiles.SelectedItem = null;
            listView1.Items.Clear();
            TypeOfProfiles.Items.Clear();
            FillDropDowns();
        }

        //Sposób wyświetlania informacji w zakładce "Wyniki"
        public void nosnoscWytezenie(decimal wynik)
        {
            if (wynik < 1)
            {
                progressBar1.Value = (Convert.ToInt16(wynik * 100));
                LabelMc.BackColor = Color.WhiteSmoke;
                LabelMc.Text = progressBar1.Value.ToString("0.00") + "%";
            }
            else
            {
                progressBar1.Value = 100;
                LabelMc.BackColor = Color.Red;
                LabelMc.Text = Convert.ToString((wynik * 100).ToString("0.0") + "%");
                MessageBox.Show("Przekroczona nośność przekroju na zginanie. Wytężenie: " + LabelMc.Text, "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        public void nosnoscWytezenieVc(decimal wynik)
        {
            if (wynik < 1)
            {
                progressBar3.Value = (Convert.ToInt16(wynik * 100));
                LabelVc.BackColor = Color.WhiteSmoke;
                LabelVc.Text = progressBar3.Value.ToString("0.00") + "%";
            }
            else
            {
                progressBar3.Value = 100;
                LabelVc.BackColor = Color.Red;
                LabelVc.Text = Convert.ToString((wynik * 100).ToString("0.0") + "%");
                MessageBox.Show("Przekroczona nośność przekroju na ścinanie. Wytężenie: " + LabelVc.Text, "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        public void nosnoscWytezenieSc(decimal wynik)
        {
            if (wynik < 1)
            {
                progressBar2.Value = (Convert.ToInt16(wynik * 100));
                LabelSc.BackColor = Color.WhiteSmoke;
                LabelSc.Text = progressBar2.Value.ToString("0.00") + "%";
            }
            else
            {
                progressBar2.Value = 100;
                LabelSc.BackColor = Color.Red;
                LabelSc.Text = Convert.ToString((wynik * 100).ToString("0.0") + "%");
                MessageBox.Show("Przekroczona nośność przekroju na ściskanie. Wytężenie: " + LabelSc.Text, "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        public void wypiszNosnosciPrzekroju()
        {
            textBoxMc.Text = Convert.ToString(z.NosnoscZginanie().ToString("0.00"));
            textBoxVc.Text = Convert.ToString(z.NosnoscScinanie().ToString("0.00"));
            textBoxNc.Text = Convert.ToString(z.NosnoscSciskanie().ToString("0.00"));
            textBoxNr.Text = Convert.ToString(z.NosnoscRozciaganie().ToString("0.00"));
        }


    }
}
