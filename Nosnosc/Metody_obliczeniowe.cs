﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nosnosc.Utils;
using Nosnosc.Profiles;
using System.Data.SqlClient;

namespace Nosnosc
{
    public  class Metody_obliczeniowe
    {
        //przekazanie wartości z ValueProfiles
        public ValueProfiles ValueProfiles_2 { get; set; }

        ObiektWyniku wynik;
        public Metody_obliczeniowe()
        {
        }

        public Metody_obliczeniowe(ValueProfiles values, decimal fy, decimal s)
        {
            this.ValueProfiles_2 = values;
            this.Fy = fy;
            this.S = s;
        }

        public Metody_obliczeniowe(double l_1, double p_1, double q_1, double m_1)
        {
            L = l_1;
            P = p_1;
            Q = q_1;
            M = m_1;
        }
 
        //Zmienne
        public double L { get; set;}
        public double P { get; set;}
        public double Q { get; set;}
        public double M { get; set;}
        public decimal Fy { get; set; }
        public decimal S { get; set; }


        #region Schematy obliczeniowe belek

        public ObiektWyniku  Oblicz(Opcje opcje)
        {
            switch (opcje)
            {
                case Opcje.Moment:
                    return Moment();
                case Opcje.Po_trojkacie:
                    return Po_trojkacie();
                case Opcje.Sila_rozłożona:
                    return Sila_rozłożona();
                case Opcje.Sila_skupiona:
                    return Sila_skupiona();
            }
            return new ObiektWyniku(0, 0, 0);
        }

        private ObiektWyniku Sila_skupiona()
        {
            double p_2 = P / 2.0;
            ObiektWyniku wyn = new ObiektWyniku((P * L) / 4.0, p_2, p_2);
            return wyn;                                 
        }

        private ObiektWyniku Sila_rozłożona()
        {
            return new ObiektWyniku((Math.Pow(L, 2) * Q) / 8.0, (Q * L) / 2.0, (Q * L) / 2.0);
        }

        private ObiektWyniku Po_trojkacie()
       {

            double x = 0.57735 * L;
            double p = Q * L;
            double siła_tnaca_1 = (0.5 * p) * (2.0/3.0);
            double siła_tnaca_2 = (0.5 * p) * (1.0/3.0);
            double moment = ((1.0 / 3.0) * (p / 2.0)) * x - (Q * Math.Pow(x, 3)) / (6.0 * L);
            return new ObiektWyniku(moment, siła_tnaca_1, siła_tnaca_2);    
        }

        private ObiektWyniku Moment()
        {
            return new ObiektWyniku(M / 2.0, M / L, M/L);
        }
        #endregion

        public decimal NosnoscMc(double Moment_1)
        {
            decimal Mc;
            double P = 0.001;
            Mc = (this.ValueProfiles_2.Wply * Fy) * Convert.ToDecimal(P);
            decimal Mr = (Convert.ToDecimal(Moment_1) / Mc);
            return Mr;
                      
        }

        public decimal NosnoscNc(double Sila_tnaca_1, double Sila_tnaca_2)
        {
            decimal Vc = this.NosnoscScinanie();
            decimal Vr; 
            if (Sila_tnaca_1 >= Sila_tnaca_2)
            {
                 Vr = (Convert.ToDecimal(Sila_tnaca_1) / Vc);
            }
            else {
                 Vr = (Convert.ToDecimal(Sila_tnaca_2) / Vc);
            }        
            return Vr;
        }

        public decimal NosnoscSciskanie_2()
        {
            decimal Sc =  this.NosnoscSciskanie();
            return Convert.ToDecimal(this.S)/Sc;
        }


        public decimal NosnoscZginanie() {
            decimal Mc_przekroju;
            double P = 0.001;
            Mc_przekroju = (this.ValueProfiles_2.Wply * Fy) * Convert.ToDecimal(P);
            return Mc_przekroju;
        }

        public decimal NosnoscScinanie() {
            double P = 0.001;
            decimal A = this.ValueProfiles_2.A * 100;
            decimal b = this.ValueProfiles_2.b;
            decimal tf = this.ValueProfiles_2.tf;
            decimal tw = this.ValueProfiles_2.tw;
            decimal Av = A - (2 * b * tf) + (tw * tf);
            decimal Pierwiastek = Convert.ToDecimal(Math.Sqrt(3));
            decimal Vc = (Av * (Fy / Pierwiastek)) * Convert.ToDecimal(P);
            return Vc;        
        }


        public decimal NosnoscSciskanie() {

        double P = 0.001;
        decimal A = this.ValueProfiles_2.A * 100;
        decimal Nc = (A * Fy) * Convert.ToDecimal(P);
        return Nc;
        }


        public decimal NosnoscRozciaganie()
        {
            double P = 0.001;
            decimal A = this.ValueProfiles_2.A * 100;
            decimal Nr = (A * Fy) * Convert.ToDecimal(P);
            return Nr;
        }
















    }
}
