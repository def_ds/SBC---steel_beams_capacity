﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nosnosc
{
    public class ObiektWyniku
    {
        
        public double Moment_1 { get; private set; }
        public double Sila_tnaca_1 { get; private set; }
        public double Sila_tnaca_2 { get; private set; }

        public ObiektWyniku(double moment1, double silaTnaca1)
        {
            this.Moment_1 = moment1;
            this.Sila_tnaca_1 = silaTnaca1;
        }

        public ObiektWyniku(double moment1, double silaTnaca1, double silaTnaca2):this(moment1, silaTnaca1)
        {
            this.Sila_tnaca_2 = silaTnaca2;
        }
    }
}
